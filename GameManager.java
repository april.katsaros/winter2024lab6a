public class GameManager{
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	public GameManager(){
		this.drawPile = new Deck();
		this.drawPile.shuffle();
		this.centerCard = this.drawPile.drawTopCard();
		this.playerCard = this.drawPile.drawTopCard();
	}
	
	public Deck getDrawPile(){
		return drawPile;
	}
	
	public String toString(){
		String border = "-------------------------------";
		return border + "\nCenter card: " + this.centerCard + "\nPlayer card: " + this.playerCard + "\n" + border;
	}
	//shuffles and draws a new card from the pile
	public void dealCards(){
		this.drawPile.shuffle();
		this.centerCard = this.drawPile.drawTopCard();
		this.drawPile.shuffle();
		this.playerCard = this.drawPile.drawTopCard();
	}
  
	public int getNumberOfCards(){
		return this.drawPile.length();
	}
	//returns 4 if the value of the two cards are the same, 2 if the suits are the same, and -1 if neither are
	public int calculatePoints(){
		if (this.centerCard.getValue() == this.playerCard.getValue()){
			return 4;
		}
		else if (this.centerCard.getSuit() == this.playerCard.getSuit()){
			return 2;
		}
		else {
			return -1;
		}
	}
	
}