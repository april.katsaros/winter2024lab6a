import java.util.Scanner;

public class LuckyCardGameApp{
	public static void main(String[] args){
		GameManager manager = new GameManager();
		int totalPoints = 0;
		int roundNumber = 1;
		System.out.println("Welcome to the Lucky Card Game!");
		//runs game while there are cards in the deck and while the player has less than 5 points
		while (manager.getDrawPile().length() > 1 && totalPoints < 5){
			//prints round number and the two cards drawn using the GameManager toString()
			System.out.println("Round: " + roundNumber);
			System.out.println(manager);
			
			totalPoints += manager.calculatePoints();
			String endOfRoundBorder = "******************************************";
			System.out.println("Your points after round " + roundNumber + ": " + totalPoints + "\n" + endOfRoundBorder);

			manager.dealCards();
			roundNumber++;
		}
		//prints results of game; player loses if they have less than five points
		if (totalPoints == 1 || totalPoints == -1){
			System.out.println("Player loses with: " + totalPoints + " point");
		}
		else if (totalPoints < 5){
			System.out.println("Player loses with: " + totalPoints + " points");
		}
		else {
			System.out.println("Player wins with: " + totalPoints + " points");
		}
	}
}